package co.study.nh.v1.nhinit.model.pincard;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "핀-카드 직접발급 Response DTO")
public class IssuePinCardResponseDTO {

    @ApiModelProperty(value = "API Header Response 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "등록번호")
    @JsonProperty(value = "Rgno")
    private String Rgno;
}
