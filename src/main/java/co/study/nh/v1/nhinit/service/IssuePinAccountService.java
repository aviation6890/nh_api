package co.study.nh.v1.nhinit.service;

import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountResponseDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface IssuePinAccountService {
    ResponseEntity<IssuePinAccountResponseDTO> createPinAccount(IssuePinAccountRequestDTO param);

    ResponseEntity<CheckPinAccountResponseDTO> checkPinAccount(CheckPinAccountRequestDTO param);
}
