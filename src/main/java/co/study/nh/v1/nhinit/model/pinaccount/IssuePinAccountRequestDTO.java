package co.study.nh.v1.nhinit.model.pinaccount;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "핀-어카운트 직접발급. Param DTO")
public class IssuePinAccountRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "출금이체등록여부", example = "Y",notes = "Y:등록, N:미등록", required = true)
    @JsonProperty(value = "DrtrRgyn")
    private String DrtrRgyn;

    @ApiModelProperty(value = "생년월일(사업자번호) YYYYMMDD", example = "19500901", notes = "개인:YYYYMMDD, 기업:사업자번호", required = true)
    @JsonProperty(value = "BrdtBrno")
    private String BrdtBrno;

    @ApiModelProperty(value = "은행코드", example = "011", required = true, notes = "(농협은행:011, 상호금융:012)")
    @JsonProperty(value = "Bncd")
    private String Bncd;

    @ApiModelProperty(value = "계좌번호", example = "3020000003674", required = true)
    @JsonProperty(value = "Acno")
    private String Acno;
}
