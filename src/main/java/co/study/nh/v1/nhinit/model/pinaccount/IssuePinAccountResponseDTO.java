package co.study.nh.v1.nhinit.model.pinaccount;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "핀-어카운트 직접발급. Response DTO")
public class IssuePinAccountResponseDTO {

    @ApiModelProperty(value = "API Header Response 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "핀-어카운트 등록번호.")
    @JsonProperty(value = "Rgno")
    private String Rgno; //핀-어카운트 등록번호.
}
