package co.study.nh.v1.nhinit.model.pinaccount;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "핀-어카운트 직접발급 확인 Request DTO")
public class CheckPinAccountRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "등록번호", example = "발급받은 등록번호를 입력해주세요.", required = true)
    @JsonProperty(value = "Rgno")
    private String Rgno;

    @ApiModelProperty(value = "생년월일(사업자번호)", example = "19500901",notes = "개인:YYYYMMDD ,기업:사업자번호", required = true)
    @JsonProperty(value = "BrdtBrno")
    private String BrdtBrno;


}
