package co.study.nh.v1.nhinit.service.impl;

import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountResponseDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountResponseDTO;
import co.study.nh.v1.nhinit.model.pincard.CheckPinCardRequestDTO;
import co.study.nh.v1.nhinit.model.pincard.CheckPinCardResponseDTO;
import co.study.nh.v1.nhinit.model.pincard.IssuePinCardRequestDTO;
import co.study.nh.v1.nhinit.model.pincard.IssuePinCardResponseDTO;
import co.study.nh.v1.nhinit.service.IssuePinAccountService;
import co.study.nh.v1.nhinit.service.IssuePinCardService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class IssuePinCardServiceImpl implements IssuePinCardService {

    private static String ISSUE_FIN_CARD_URL = "https://developers.nonghyup.com/OpenFinCardDirect.nh";      // 1.3 핀-카드 직접발급.
    private static String CHECK_FIN_CARD_URL = "https://developers.nonghyup.com/CheckOpenFinCardDirect.nh";  // 1.4 핀-카드 직접발급 확인.

    HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * 1.3 핀- 카드 직접 발급.
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<IssuePinCardResponseDTO> createPinCard(IssuePinCardRequestDTO param) {
        log.info("CALL API URL ::{}", ISSUE_FIN_CARD_URL);
        log.info("param ::{}", param);

        RestTemplate restTemplate = new RestTemplate();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<IssuePinCardResponseDTO> exchange = restTemplate
                .exchange(ISSUE_FIN_CARD_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        IssuePinCardResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }


    /**
     * 1.4 핀- 카드 직접 발급 확인.
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<CheckPinCardResponseDTO> checkPinCard(CheckPinCardRequestDTO param) {
        log.info("CALL API URL ::{}", CHECK_FIN_CARD_URL);
        log.info("param ::{}", param);

        RestTemplate restTemplate = new RestTemplate();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<CheckPinCardResponseDTO> exchange = restTemplate
                .exchange(CHECK_FIN_CARD_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        CheckPinCardResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

}
