package co.study.nh.v1.nhinit.model.pinaccount;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "핀-어카운트 직접발급 확인 Response DTO")
public class CheckPinAccountResponseDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "핀-어카운트", required = true)
    @JsonProperty(value = "FinAcno")
    private String FinAcno;

    @ApiModelProperty(value = "등록일자", required = true)
    @JsonProperty(value = "RgsnYmd")
    private String RgsnYmd;
}
