package co.study.nh.v1.nhinit.controller;

import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountResponseDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountResponseDTO;
import co.study.nh.v1.nhinit.model.pincard.CheckPinCardRequestDTO;
import co.study.nh.v1.nhinit.model.pincard.CheckPinCardResponseDTO;
import co.study.nh.v1.nhinit.model.pincard.IssuePinCardRequestDTO;
import co.study.nh.v1.nhinit.model.pincard.IssuePinCardResponseDTO;
import co.study.nh.v1.nhinit.service.IssuePinAccountService;
import co.study.nh.v1.nhinit.service.IssuePinCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@Api(tags = {"1.시작하기"})
public class IssuePinController {

    private final IssuePinAccountService issuePinAccountService;
    private final IssuePinCardService issuePinCardService;

    @ApiOperation(value = "1.1 핀-어카운트 직접발급.")
    @PostMapping(value = "/v1/OpenFinAccountDirect", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IssuePinAccountResponseDTO> createPinAccount(@RequestBody IssuePinAccountRequestDTO param) {
        return issuePinAccountService.createPinAccount(param);
    }

    @ApiOperation(value = "1.2 핀-어카운트 직접발급 확인.")
    @PostMapping(value = "/v1/CheckOpenFinAccountDirect", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CheckPinAccountResponseDTO> checkPinAccount(@RequestBody CheckPinAccountRequestDTO param) {
        return issuePinAccountService.checkPinAccount(param);
    }

    @ApiOperation(value = "1.3 핀-카드 직접발급.")
    @PostMapping(value = "/v1/OpenFinCardDirect", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IssuePinCardResponseDTO> createPinCard(@RequestBody IssuePinCardRequestDTO param) {
        return issuePinCardService.createPinCard(param);
    }

    @ApiOperation(value = "1.4 핀-카드 직접발급 확인.")
    @PostMapping(value = "/v1/CheckOpenFinCardDirect", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CheckPinCardResponseDTO> checkPinCard(@RequestBody CheckPinCardRequestDTO param) {
        return issuePinCardService.checkPinCard(param);
    }

}
