package co.study.nh.v1.nhinit.service.impl;

import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.CheckPinAccountResponseDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountResponseDTO;
import co.study.nh.v1.nhinit.service.IssuePinAccountService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class IssuePinAccountServiceImpl implements IssuePinAccountService {

    private static String ISSUE_FIN_ACCOUNT_URL = "https://developers.nonghyup.com/OpenFinAccountDirect.nh";      // 1.1 핀-어카운트 직접발급.
    private static String CHECK_FIN_ACCOUNT_URL = "https://developers.nonghyup.com/CheckOpenFinAccountDirect.nh";  // 1.2 핀-어카운트 직접발급 확인.

    HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * 1.1 핀- 어카운트 직접 발급.
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<IssuePinAccountResponseDTO> createPinAccount(IssuePinAccountRequestDTO param) {

        log.info("CALL API URL ::{}", ISSUE_FIN_ACCOUNT_URL);
        log.info("param ::{}", param);

        RestTemplate restTemplate = new RestTemplate();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<IssuePinAccountResponseDTO> exchange = restTemplate
                .exchange(ISSUE_FIN_ACCOUNT_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        IssuePinAccountResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 1.2 핀- 어카운트 직접 발급 확인.
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<CheckPinAccountResponseDTO> checkPinAccount(CheckPinAccountRequestDTO param) {
        log.info("CALL API URL ::{}", CHECK_FIN_ACCOUNT_URL);
        log.info("param ::{}", param);

        RestTemplate restTemplate = new RestTemplate();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<CheckPinAccountResponseDTO> exchange = restTemplate
                .exchange(CHECK_FIN_ACCOUNT_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        CheckPinAccountResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }
}
