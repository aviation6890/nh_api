package co.study.nh.v1.nhinit.service;

import co.study.nh.v1.nhinit.model.pincard.CheckPinCardRequestDTO;
import co.study.nh.v1.nhinit.model.pincard.CheckPinCardResponseDTO;
import co.study.nh.v1.nhinit.model.pincard.IssuePinCardRequestDTO;
import co.study.nh.v1.nhinit.model.pincard.IssuePinCardResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface IssuePinCardService {

    ResponseEntity<IssuePinCardResponseDTO> createPinCard(IssuePinCardRequestDTO param);

    ResponseEntity<CheckPinCardResponseDTO> checkPinCard(CheckPinCardRequestDTO param);
}
