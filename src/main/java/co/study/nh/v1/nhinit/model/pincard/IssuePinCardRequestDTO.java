package co.study.nh.v1.nhinit.model.pincard;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "핀-카드 직접발급 Param DTO")
public class IssuePinCardRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "생년월일", example = "19500901", notes = "YYYYMMDD", required = true)
    @JsonProperty(value = "Brdt")
    private String Brdt;

    @ApiModelProperty(value = "카드번호", example = "카드번호를 입력해주세요.", notes = "테스트 환경일때 마이페이지> 서비스관리에서 확인 가능.", required = true)
    @JsonProperty(value = "Cano")
    private String Cano;

}
