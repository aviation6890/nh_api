package co.study.nh.v1.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class APICommonHeader {

    @ApiModelProperty(value = "api명", example = "API 명을 입력해주세요.", required = true)
    @JsonProperty(value = "ApiNm")
    private String ApiNm;

    @ApiModelProperty(value = "전송일자", example = "20210304", required = true)
    @JsonProperty(value = "Tsymd")
    private String Tsymd;

    @ApiModelProperty(value = "전송시각", example = "112800", required = true)
    @JsonProperty(value = "Trtm")
    private String Trtm;

    @ApiModelProperty(value = "기관코드", example = "발급받은 기관코드를 넣어주세요.", required = true)
    @JsonProperty(value = "Iscd")
    private String Iscd;

    @ApiModelProperty(value = "핀테크 일련번호", example = "001", required = true)
    @JsonProperty(value = "FintechApsno")
    private String FintechApsno;

    @ApiModelProperty(value = "api서비스코드", example = "DrawingTransferA", required = true)
    @JsonProperty(value = "ApiSvcCd")
    private String ApiSvcCd;

    @ApiModelProperty(value = "기관거래고유번호", example = "2021030400000000004", required = true)
    @JsonProperty(value = "IsTuno")
    private String IsTuno;

    @ApiModelProperty(value = "인증키", example = "api 발급 받은 인증키를 넣어주세요.", required = true)
    @JsonProperty(value = "AccessToken")
    private String AccessToken;
}
