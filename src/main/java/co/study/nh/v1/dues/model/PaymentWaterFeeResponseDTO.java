package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "상하수도요금납부 선조회 Response DTO")
public class PaymentWaterFeeResponseDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "공과금 지로번호", required = true)
    @JsonProperty(value = "PbtxGroNo")
    private String PbtxGroNo;

    @ApiModelProperty(value = "공과금 기관분류코드", required = true)
    @JsonProperty(value = "PbtxInstClasCd")
    private String PbtxInstClasCd;

    @ApiModelProperty(value = "공과금 납세번호", required = true)
    @JsonProperty(value = "PbtxPytxno")
    private String PbtxPytxno;

    @ApiModelProperty(value = "공과금 전자납부번호", required = true)
    @JsonProperty(value = "PbtxElcrPmntNo")
    private String PbtxElcrPmntNo;

    @ApiModelProperty(value = "납부기록사항", required = true)
    @JsonProperty(value = "PayRcrdSbjcCntn")
    private String PayRcrdSbjcCntn;

    @ApiModelProperty(value = "납부금액", required = true)
    @JsonProperty(value = "PryAmt")
    private String PryAmt;

    @ApiModelProperty(value = "기관명", required = true)
    @JsonProperty(value = "Isnm")
    private String Isnm;

    @ApiModelProperty(value = "납기내일자", required = true)
    @JsonProperty(value = "TmpmWthnDay")
    private String TmpmWthnDay;

    @ApiModelProperty(value = "납부자명", required = true)
    @JsonProperty(value = "PryNm")
    private String PryNm;

}

