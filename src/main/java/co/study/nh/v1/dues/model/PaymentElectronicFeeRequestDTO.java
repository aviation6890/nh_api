package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전기요금납부 Request DTO")
public class PaymentElectronicFeeRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "전자납부번호", example = "0606628088", required = true, notes = "테스트에서는 '0606628088' 고정값으로 입력")
    @JsonProperty(value = "ElecPayNo")
    private String ElecPayNo;

    @ApiModelProperty(value = "계좌번호", example = "3020000000071", required = true, notes = "테스트에서는 '3020000000071' 고정값으로 입력")
    @JsonProperty(value = "Acno")
    private String Acno;

}
