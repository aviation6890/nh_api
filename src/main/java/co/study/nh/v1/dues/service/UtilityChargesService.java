package co.study.nh.v1.dues.service;

import co.study.nh.v1.dues.model.*;
import co.study.nh.v1.easypay.model.WithdrawRequestDTO;
import co.study.nh.v1.easypay.model.WithdrawResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface UtilityChargesService {

    // 5.1 상하수도요금납부 선조회.
    ResponseEntity<WithdrawResponseDTO> inquiryWaterFee(WithdrawRequestDTO param);

    // 5.2 상하수도요금납부.
    ResponseEntity<PaymentWaterFeeResponseDTO> paymentWaterFee(PaymentWaterFeeRequestDTO param);

    // 5.3 상하수도요금납부내역조회.
    ResponseEntity<PaymentWaterFeeHistoryResponseDTO> paymentWaterFeesHistory(PaymentWaterFeeHistoryRequestDTO param);

    // 5.4 전기요금납부 선조회.
    ResponseEntity<InquiryElectronicFeeResponseDTO> inquiryElectronicFee(InquiryElectronicFeeRequestDTO param);

    // 5.5 전기요금납부.
    ResponseEntity<PaymentElectronicFeeResponseDTO> paymentElectronicFee(PaymentElectronicFeeRequestDTO param);

    // 5.6 전기요금납부내역조회.
    ResponseEntity<PaymentElectronicFeeHistoryResponseDTO> paymentElectronicFeeHistory(PaymentElectronicFeeHistoryRequestDTO param);
}
