package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "상하수도요금납부 선조회 Response DTO")
public class InquiryWaterFeeResponseDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "핀-어카운트", required = true, notes = "전체 건수")
    @JsonProperty(value = "TotCnt")
    private String TotCnt;

    @ApiModelProperty(value = "조회총건수", required = true, notes = "페이지 당 조회된 총건수")
    @JsonProperty(value = "IqtCnt")
    private String IqtCnt;

    @ApiModelProperty(value = "연속데이터여부", required = true, notes = "Y : 데이터 존재, N : 데이터 미존재")
    @JsonProperty(value = "CtntDataYn")
    private String CtntDataYn;

    @ApiModelProperty(value = "상하수도목록", required = true, notes = "\t페이지당 10건")
    @JsonProperty(value = "REC")
    private String REC;

    @ApiModelProperty(value = "공과금 지로번호", required = true)
    @JsonProperty(value = "PbtxGroNo")
    private String PbtxGroNo;

    @ApiModelProperty(value = "공과금 기관분류코드", required = true)
    @JsonProperty(value = "PbtxInstClasCd")
    private String PbtxInstClasCd;

    @ApiModelProperty(value = "공과금 전자납부번호", required = true)
    @JsonProperty(value = "PbtxElcrPmntNo")
    private String PbtxElcrPmntNo;

    @ApiModelProperty(value = "기관명", required = true)
    @JsonProperty(value = "Isnm")
    private String Isnm;

    @ApiModelProperty(value = "세목명", required = true)
    @JsonProperty(value = "TxitNm")
    private String TxitNm;

    @ApiModelProperty(value = "공과금 고지형태", required = true)
    @JsonProperty(value = "PbtxNtfFrmt")
    private String PbtxNtfFrmt;

    @ApiModelProperty(value = "거래금액", required = true)
    @JsonProperty(value = "Tram")
    private String Tram;

    @ApiModelProperty(value = "공과금 납기구분", required = true)
    @JsonProperty(value = "PbtxTmpmDsnc")
    private String PbtxTmpmDsnc;

    @ApiModelProperty(value = "공과금 납부기한", required = true)
    @JsonProperty(value = "TmpmWthnDay")
    private String TmpmWthnDay;

    @ApiModelProperty(value = "공과금 선결제입금구분코드", required = true)
    @JsonProperty(value = "PbtxAhstRcvDvCd")
    private String PbtxAhstRcvDvCd;

    @ApiModelProperty(value = "공과금 자동이체등록여부", required = true)
    @JsonProperty(value = "PbtxAttrRgsnYn")
    private String PbtxAttrRgsnYn;

}

