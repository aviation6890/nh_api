package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전기요금납부 내역조회 Response DTO")
public class PaymentElectronicFeeHistoryResponseDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "총건수", required = true, notes = "전체 건수")
    @JsonProperty(value = "TotCnt")
    private String TotCnt;

    @ApiModelProperty(value = "조회총건수", required = true, notes = "페이지 당 조회된 총건수")
    @JsonProperty(value = "IqtCnt")
    private String IqtCnt;

    @ApiModelProperty(value = "페이지번호", required = true)
    @JsonProperty(value = "PageNo")
    private String PageNo;

    @ApiModelProperty(value = "연속데이터여부", required = true, notes = "Y : 데이터 존재, N : 데이터 미존재")
    @JsonProperty(value = "CtntDataYn")
    private String CtntDataYn;

    @ApiModelProperty(value = "납부내역목록", required = true, notes = "페이지당 20건")
    @JsonProperty(value = "REC")
    private String REC;

    @ApiModelProperty(value = "전자납부번호", required = true)
    @JsonProperty(value = "ElcrPmntNo")
    private String ElcrPmntNo;

    @ApiModelProperty(value = "출금계좌번호", required = true)
    @JsonProperty(value = "Drano")
    private String Drano;

    @ApiModelProperty(value = "납부상태코드", required = true, notes = "0 : 처리 중, 1 : 정상, 9 : 오류")
    @JsonProperty(value = "PmntStcd")
    private String PmntStcd;

    @ApiModelProperty(value = "납부금액", required = true)
    @JsonProperty(value = "PmntAmt")
    private String PmntAmt;

    @ApiModelProperty(value = "납부일자", required = true, notes = "YYYYMMDD")
    @JsonProperty(value = "PmntYmd")
    private String PmntYmd;

    @ApiModelProperty(value = "납부시각", required = true, notes = "HH24MISS")
    @JsonProperty(value = "PmntTm")
    private String PmntTm;

    @ApiModelProperty(value = "납부유형", required = true, notes = "E : 전기, S : 상하수도")
    @JsonProperty(value = "PmntPtrn")
    private String PmntPtrn;

    @ApiModelProperty(value = "납세자명", required = true)
    @JsonProperty(value = "Txpr")
    private String Txpr;

    @ApiModelProperty(value = "과세기관", required = true)
    @JsonProperty(value = "TxtnIntt")
    private String TxtnIntt;

    @ApiModelProperty(value = "납부기한", required = true)
    @JsonProperty(value = "PmntDln")
    private String PmntDln;

    @ApiModelProperty(value = "기록사항", required = true, notes = "입금정보")
    @JsonProperty(value = "RcrdSbjc")
    private String RcrdSbjc;
}

