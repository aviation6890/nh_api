package co.study.nh.v1.dues.controller;

import co.study.nh.v1.dues.model.*;
import co.study.nh.v1.dues.service.UtilityChargesService;
import co.study.nh.v1.easypay.model.WithdrawRequestDTO;
import co.study.nh.v1.easypay.model.WithdrawResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@Api(tags = {"5.지로/공과금"})
public class UtilityChargesController {

    private final UtilityChargesService utilityChargesService;

    @ApiOperation(value = "5.1 상하수도요금납부 선조회")
    @PostMapping(value = "/v1/InquireSewageFarePayment", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WithdrawResponseDTO> inquiryWaterFee(@RequestBody WithdrawRequestDTO param) {
        return utilityChargesService.inquiryWaterFee(param);
    }

    @ApiOperation(value = "5.2 상하수도 요금납부.")
    @PostMapping(value = "/v1/SewageFarePayment", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentWaterFeeResponseDTO> paymentsWaterFee(@RequestBody PaymentWaterFeeRequestDTO param) {
        return utilityChargesService.paymentWaterFee(param);
    }

    @ApiOperation(value = "5.3 상하수도요금 납부내역조회.")
    @PostMapping(value = "/v1/InquireSewageFarePaymentHistory", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentWaterFeeHistoryResponseDTO> paymentsWaterFeeHistory(@RequestBody PaymentWaterFeeHistoryRequestDTO param) {
        return utilityChargesService.paymentWaterFeesHistory(param);
    }

    @ApiOperation(value = "5.4 전기요금납부 선조회.")
    @PostMapping(value = "/v1/InquireElectricityFarePayment", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InquiryElectronicFeeResponseDTO> inquiryElectronicFee(@RequestBody InquiryElectronicFeeRequestDTO param) {
        return utilityChargesService.inquiryElectronicFee(param);
    }

    @ApiOperation(value = "5.5 전기요금납부.")
    @PostMapping(value = "/v1/ElectricityFarePayment", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentElectronicFeeResponseDTO> paymentElectronicFee(@RequestBody PaymentElectronicFeeRequestDTO param) {
        return utilityChargesService.paymentElectronicFee(param);
    }

    @ApiOperation(value = "5.6 전기요금납부내역조회.")
    @PostMapping(value = "/v1/InquireElectricityFarePaymentHistory", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentElectronicFeeHistoryResponseDTO> paymentElectronicFeeHistory(@RequestBody PaymentElectronicFeeHistoryRequestDTO param) {
        return utilityChargesService.paymentElectronicFeeHistory(param);
    }

}
