package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전기요금납부 내역조회 Request DTO")
public class PaymentElectronicFeeHistoryRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "전자납부번호", example = "2632001709000428753", notes = "미입력 시 기간 내 납부내역 전체 조회 테스트에서는 '0606628088' 고정값으로 입력")
    @JsonProperty(value = "ElecPayNo")
    private String ElecPayNo;

    @ApiModelProperty(value = "페이지번호", example = "1", notes = "Default : 1 테스트에서는 '1' 고정값으로 입력")
    @JsonProperty(value = "PageNo")
    private String PageNo;

    @ApiModelProperty(value = "조회시작일자", required = true, example = "20191010", notes = "YYYYMMDD 테스트에서는 '20191010' 고정값으로 입력")
    @JsonProperty(value = "Insymd")
    private String Insymd;

    @ApiModelProperty(value = "조회종료일자", required = true, example = "20191010", notes = "YYYYMMDD 테스트에서는 '20191010' 고정값으로 입력")
    @JsonProperty(value = "Ineymd")
    private String Ineymd;

}
