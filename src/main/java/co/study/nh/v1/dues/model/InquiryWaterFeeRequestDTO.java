package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "상하수도요금납부 선조회 Request DTO")
public class InquiryWaterFeeRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "전자수용가번호", example = "263201755751004", required = true, notes = "테스트에서는 '263201755751004' 고정값으로 입력")
    @JsonProperty(value = "ElecCsmrNo")
    private String ElecCsmrNo;

    @ApiModelProperty(value = "계좌번호", example = "3020000000071", required = true, notes = "테스트에서는 '3020000000071' 고정값으로 입력")
    @JsonProperty(value = "Acno")
    private String Acno;

    @ApiModelProperty(value = "페이지번호", example = "1", notes = "미입력시 default 1 세팅 테스트에서는 '1' 고정값으로 입력")
    @JsonProperty(value = "PageNo")
    private String PageNo;

}
