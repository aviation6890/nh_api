package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "상하수도요금납부 선조회 Request DTO")
public class PaymentWaterFeeRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "공과금 지로번호", example = "1004102", required = true, notes = "납부선조회 응답의 PbtxGroNo 값 테스트에서는 '1004102' 고정값으로 입력")
    @JsonProperty(value = "PbtxGroNo")
    private String PbtxGroNo;

    @ApiModelProperty(value = "공과금 기관분류코드", example = "26", required = true, notes = "납부선조회 응답의  PbtxInstClasCd  값 테스트에서는 '26' 고정값으로 입력")
    @JsonProperty(value = "PbtxInstClasCd")
    private String PbtxInstClasCd;

    @ApiModelProperty(value = "공과금 납세번호", example = "2632017557510042632000017091002", notes = "수납부선조회 응답의  PbtxPytxno 값 테스트에서는 '2632017557510042632000017091002' 고정값으로 입력")
    @JsonProperty(value = "PbtxPytxno")
    private String PbtxPytxno;

    @ApiModelProperty(value = "공과금 전자납부번호", example = "2632001709000428753", required = true, notes = "납부선조회 응답의 PbtxElcrPmntNo 값 테스트에서는 '2632001709000428753' 고정값으로 입력")
    @JsonProperty(value = "PbtxElcrPmntNo")
    private String PbtxElcrPmntNo;

    @ApiModelProperty(value = "계좌번호", example = "'3020000000071' ", required = true, notes = "테스트에서는 '3020000000071' 고정값으로 입력")
    @JsonProperty(value = "Acno")
    private String Acno;

    @ApiModelProperty(value = "납부기록사항", example = "1", notes = "테스트에서는 '주택관리공단' 고정값으로 입력")
    @JsonProperty(value = "PayRcrdSbjcCntn")
    private String PayRcrdSbjcCntn;

    @ApiModelProperty(value = "거래금액", example = "733120", required = true, notes = "납부선조회 응답의 Tram 값 테스트에서는 '733120' 고정값으로 입력")
    @JsonProperty(value = "Tram")
    private String Tram;

    @ApiModelProperty(value = "기관명", example = "부산광역시", required = true, notes = "납부선조회 응답의 Isnm  값 테스트에서는 '부산광역시' 고정값으로 입력")
    @JsonProperty(value = "Isnm")
    private String Isnm;

    @ApiModelProperty(value = "납기내일자", example = "20191130", notes = "납부선조회 응답의 TmpmWthnDay 값 테스트에서는 '20191130' 고정값으로 입력")
    @JsonProperty(value = "TmpmWthnDay")
    private String TmpmWthnDay;

    @ApiModelProperty(value = "납부자명", example = "홍길남", required = true, notes = "테스트에서는 '홍길남' 고정값으로 입력")
    @JsonProperty(value = "PryNm")
    private String PryNm;

    @ApiModelProperty(value = "세목명", example = "상하수도요금", notes = "납부선조회 응답의 TxitNm 값 테스트에서는 '상하수도요금' 고정값으로 입력")
    @JsonProperty(value = "TxitNm")
    private String TxitNm;


}
