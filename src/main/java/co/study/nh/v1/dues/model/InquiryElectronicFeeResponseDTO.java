package co.study.nh.v1.dues.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전기요금납부 선조회 Response DTO")
public class InquiryElectronicFeeResponseDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "전자납부번호", required = true)
    @JsonProperty(value = "ElecPayNo")
    private String ElecPayNo;

    @ApiModelProperty(value = "회원성명", required = true)
    @JsonProperty(value = "MmbrFlnm")
    private String MmbrFlnm;

    @ApiModelProperty(value = "공과금 지로번호", required = true)
    @JsonProperty(value = "PbtxGroNo")
    private String PbtxGroNo;

    @ApiModelProperty(value = "공과금 지사명", required = true)
    @JsonProperty(value = "PbtxBrnNm")
    private String PbtxBrnNm;

    @ApiModelProperty(value = "고객조회번호", required = true)
    @JsonProperty(value = "CustInqNo")
    private String CustInqNo;

    @ApiModelProperty(value = "과세년월", required = true)
    @JsonProperty(value = "TaxtYm")
    private String TaxtYm;

    @ApiModelProperty(value = "납부금액", required = true)
    @JsonProperty(value = "Tram")
    private String Tram;

    @ApiModelProperty(value = "공과금 납부기한", required = true)
    @JsonProperty(value = "PbtxPayExdt")
    private String PbtxPayExdt;

    @ApiModelProperty(value = "고지형태", required = true)
    @JsonProperty(value = "NtfFrmt")
    private String NtfFrmt;

    @ApiModelProperty(value = "발행형태", required = true)
    @JsonProperty(value = "IssShp")
    private String IssShp;

    @ApiModelProperty(value = "기타", required = true)
    @JsonProperty(value = "Etc")
    private String Etc;

    @ApiModelProperty(value = "공과금 납기구분", required = true)
    @JsonProperty(value = "PbtxTmpmDsnc")
    private String PbtxTmpmDsnc;
}

