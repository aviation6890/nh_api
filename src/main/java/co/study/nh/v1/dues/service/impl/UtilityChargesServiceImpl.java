package co.study.nh.v1.dues.service.impl;

import co.study.nh.v1.dues.model.*;
import co.study.nh.v1.dues.service.UtilityChargesService;
import co.study.nh.v1.easypay.model.WithdrawRequestDTO;
import co.study.nh.v1.easypay.model.WithdrawResponseDTO;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class UtilityChargesServiceImpl implements UtilityChargesService {

    private static String INQUIRY_WATER_FEE = "https://developers.nonghyup.com/InquireSewageFarePayment.nh";                            // 4.1 상하수도요금납부 선조회.
    private static String PAYMENT_WATER_FEE = "https://developers.nonghyup.com/SewageFarePayment.nh";                                   // 4.2 상하수도요금납부
    private static String PAYMENT_WATER_FEE_HISTORY = "https://developers.nonghyup.com/InquireSewageFarePaymentHistory.nh";             // 4.3 상하수도요금 납부내역조회.
    private static String INQUIRY_ELECTRONIC_FEE = "https://developers.nonghyup.com/InquireElectricityFarePayment.nh";                  // 4.4 전기요금납부 선조회.
    private static String PAYMENT_ELECTRONIC_FEE = "https://developers.nonghyup.com/ElectricityFarePayment.nh";                         // 4.5 전기요금납부
    private static String PAYMENT_ELECTRONIC_FEE_HISTORY = "https://developers.nonghyup.com/InquireElectricityFarePayment.nh";          // 4.6 전기요금납부내역조회.

    HttpHeaders httpHeaders = new HttpHeaders();
    RestTemplate restTemplate = new RestTemplate();

    /**
     * 5.1 상하수도요급납부 선조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<WithdrawResponseDTO> inquiryWaterFee(WithdrawRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_WATER_FEE);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<WithdrawResponseDTO> exchange = restTemplate
                .exchange(INQUIRY_WATER_FEE,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        WithdrawResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 5.2 상하수도요금납부.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<PaymentWaterFeeResponseDTO> paymentWaterFee(PaymentWaterFeeRequestDTO param) {
        log.info("CALL API URL ::{}", PAYMENT_WATER_FEE);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<PaymentWaterFeeResponseDTO> exchange = restTemplate
                .exchange(PAYMENT_WATER_FEE,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        PaymentWaterFeeResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 5.3 상하수도요금납부 내역조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<PaymentWaterFeeHistoryResponseDTO> paymentWaterFeesHistory(PaymentWaterFeeHistoryRequestDTO param) {
        log.info("CALL API URL ::{}", PAYMENT_WATER_FEE_HISTORY);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<PaymentWaterFeeHistoryResponseDTO> exchange = restTemplate
                .exchange(PAYMENT_WATER_FEE_HISTORY,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        PaymentWaterFeeHistoryResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 5.4 전기요금납부 선조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<InquiryElectronicFeeResponseDTO> inquiryElectronicFee(InquiryElectronicFeeRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_ELECTRONIC_FEE);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<InquiryElectronicFeeResponseDTO> exchange = restTemplate
                .exchange(INQUIRY_ELECTRONIC_FEE,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        InquiryElectronicFeeResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 5.5 전기요금납부.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<PaymentElectronicFeeResponseDTO> paymentElectronicFee(PaymentElectronicFeeRequestDTO param) {
        log.info("CALL API URL ::{}", PAYMENT_ELECTRONIC_FEE);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<PaymentElectronicFeeResponseDTO> exchange = restTemplate
                .exchange(PAYMENT_ELECTRONIC_FEE,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        PaymentElectronicFeeResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 5.6 전기요금납부 내역 조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<PaymentElectronicFeeHistoryResponseDTO> paymentElectronicFeeHistory(PaymentElectronicFeeHistoryRequestDTO param) {
        log.info("CALL API URL ::{}", PAYMENT_ELECTRONIC_FEE_HISTORY);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<PaymentElectronicFeeHistoryResponseDTO> exchange = restTemplate
                .exchange(PAYMENT_ELECTRONIC_FEE_HISTORY,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        PaymentElectronicFeeHistoryResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }
}
