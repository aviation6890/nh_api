package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(description = "개인카드승인내역조회 Response DTO")
public class InquiryCreditCardHistoryResponseDTO {

    @ApiModelProperty(value = "API Header 응답 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "권한확인티켓", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AthrCnfrTckt")
    private String AthrCnfrTckt;

    @ApiModelProperty(value = "웹URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "WebUrl")
    private String WebUrl;

    @ApiModelProperty(value = "안드로이드설치URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AndInltUrl")
    private String AndInltUrl;

    @ApiModelProperty(value = "안드로이드앱URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AndAppUrl")
    private String AndAppUrl;

    @ApiModelProperty(value = "안드로이드웹URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AndWebUrl")
    private String AndWebUrl;

    @ApiModelProperty(value = "IOS설치URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "IosInltUrl")
    private String IosInltUrl;

    @ApiModelProperty(value = "IOS앱URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "IosAppUrl")
    private String IosAppUrl;

    @ApiModelProperty(value = "IOS웹URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "IosWebUrl")
    private String IosWebUrl;


    @ApiModelProperty(value = "연속데이터여부", required = true)
    @JsonProperty(value = "CtntDataYn")
    private String CtntDataYn;

    @ApiModelProperty(value = "총건수", required = true)
    @JsonProperty(value = "TotCnt")
    private String TotCnt;

    @ApiModelProperty(value = "조회총건수", required = true)
    @JsonProperty(value = "Iqtcnt")
    private String Iqtcnt;

    @ApiModelProperty(value = "거래내역목록", required = true, notes = "최대15건")
    @JsonProperty(value = "REC")
    private List<REC> REC;

    @Data
    private class REC {
        @ApiModelProperty(value = "카드승인번호", required = true)
        @JsonProperty(value = "CardAthzNo")
        private String CardAthzNo;

        @ApiModelProperty(value = "거래일자", required = true)
        @JsonProperty(value = "Trdd")
        private String Trdd;

        @ApiModelProperty(value = "거래시각", required = true)
        @JsonProperty(value = "Txtm")
        private String Txtm;

        @ApiModelProperty(value = "이용금액", required = true)
        @JsonProperty(value = "Usam")
        private String Usam;

        @ApiModelProperty(value = "가맹점사업자등록번호", required = true)
        @JsonProperty(value = "AfstNoBrno")
        private String AfstNoBrno;

        @ApiModelProperty(value = "가맹점번호", required = true)
        @JsonProperty(value = "AfstNo")
        private String AfstNo;

        @ApiModelProperty(value = "가맹점명", required = true)
        @JsonProperty(value = "AfstNm")
        private String AfstNm;

        @ApiModelProperty(value = "매출종류", required = true, notes = "1:일시불 2:할부 3:현금서비스 6:해외일시불 7:해외할부 8:해외현금서비스")
        @JsonProperty(value = "AmslKnd")
        private String AmslKnd;

        @ApiModelProperty(value = "할부기간", required = true)
        @JsonProperty(value = "Tris")
        private String Tris;

        @ApiModelProperty(value = "취소여부", notes = "정상:0, 취소:1")
        @JsonProperty(value = "Ccyn")
        private String Ccyn;

        @ApiModelProperty(value = "취소일자", notes = "취소시 취소일자")
        @JsonProperty(value = "CnclYmd")
        private String CnclYmd;

        @ApiModelProperty(value = "통화코드", notes = "해외사용분 통화코드")
        @JsonProperty(value = "Crcd")
        private String Crcd;

        @ApiModelProperty(value = "현지이용금액")
        @JsonProperty(value = "AcplUsam")
        private String AcplUsam;
    }
}
