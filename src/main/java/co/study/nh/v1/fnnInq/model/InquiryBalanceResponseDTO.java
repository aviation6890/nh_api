package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "잔액조회 Response DTO")
public class InquiryBalanceResponseDTO {

    @ApiModelProperty(value = "API Header 응답 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "권한확인티켓", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AthrCnfrTckt")
    private String AthrCnfrTckt;

    @ApiModelProperty(value = "웹URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "WebUrl")
    private String WebUrl;

    @ApiModelProperty(value = "안드로이드설치URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AndInltUrl")
    private String AndInltUrl;

    @ApiModelProperty(value = "안드로이드앱URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AndAppUrl")
    private String AndAppUrl;

    @ApiModelProperty(value = "안드로이드웹URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "AndWebUrl")
    private String AndWebUrl;

    @ApiModelProperty(value = "IOS설치URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "IosInltUrl")
    private String IosInltUrl;

    @ApiModelProperty(value = "IOS앱URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "IosAppUrl")
    private String IosAppUrl;

    @ApiModelProperty(value = "IOS웹URL", notes = "AI001, AI002오류 발생시 출력됨")
    @JsonProperty(value = "IosWebUrl")
    private String IosWebUrl;

    @ApiModelProperty(value = "핀-어카운트", example = "", required = true)
    @JsonProperty(value = "FinAcno")
    private String FinAcno;

    @ApiModelProperty(value = "원장잔액", example = "", required = true)
    @JsonProperty(value = "Ldbl")
    private String Ldbl;

    @ApiModelProperty(value = "실지급가능액", example = "", required = true)
    @JsonProperty(value = "RlpmAbamt")
    private String RlpmAbamt;

}
