package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "자기앞수표조회 Response DTO")
public class InquiryCheckResponseDTO {

    @ApiModelProperty(value = "API Header 응답 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "자기앞수표번호", required = true)
    @JsonProperty(value = "CschNo")
    private String CschNo;

    @ApiModelProperty(value = "발행일자", required = true)
    @JsonProperty(value = "Psymd")
    private String Psymd;

    @ApiModelProperty(value = "수표액면금액", required = true)
    @JsonProperty(value = "ChPvamt")
    private String ChPvamt;
}
