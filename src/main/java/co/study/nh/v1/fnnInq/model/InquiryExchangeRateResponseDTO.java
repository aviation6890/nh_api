package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.ArrayList;

@Data
@ApiModel(description = "환율조회 Response DTO")
public class InquiryExchangeRateResponseDTO {

    @ApiModelProperty(value = "API Header 응답 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "조회총건수", example = "", required = true)
    @JsonProperty(value = "Iqtcnt")
    private String Iqtcnt;

    @ApiModelProperty(value = "반복부", required = true, notes = "최대 100건")
    @JsonProperty(value = "REC")
    private ArrayList<REC> REC = new ArrayList<>();

    @Data
    static class REC {
        @ApiModelProperty(value = "등록회차")
        @JsonProperty(value = "RgsnTmbt")
        private String RgsnTmbt;

        @ApiModelProperty(value = "통화코드")
        @JsonProperty(value = "Crcd")
        private String Crcd;

        @ApiModelProperty(value = "현찰매입율")
        @JsonProperty(value = "CshBnrt")
        private String CshBnrt;

        @ApiModelProperty(value = "현찰매도율")
        @JsonProperty(value = "CshSlrt")
        private String CshSlrt;

        @ApiModelProperty(value = "전신환 우대매입율")
        @JsonProperty(value = "TlchPrnlBnrt")
        private String TlchPrnlBnrt;

        @ApiModelProperty(value = "전신환 우대매도율")
        @JsonProperty(value = "TlchPrnlSlrt")
        private String TlchPrnlSlrt;

        @ApiModelProperty(value = "T/C 매입율")
        @JsonProperty(value = "TcBnrt")
        private String TcBnrt;

        @ApiModelProperty(value = "T/C 매도율")
        @JsonProperty(value = "TcSlrt")
        private String TcSlrt;

        @ApiModelProperty(value = "매매기준율")
        @JsonProperty(value = "BrgnBsrt")
        private String BrgnBsrt;
    }

}
