package co.study.nh.v1.fnnInq.service;

import co.study.nh.v1.fnnInq.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface FinancialInquiryService {

    //2.1 예금주 조회
    ResponseEntity<InquireAccountResponseDTO> getInquiryAccount(InquireAccountRequestDTO param);

    //2.2 타행 예금주 조회
    ResponseEntity<InquireOrderAccountResponseDTO> getInquiryOrderAccount(InquireOrderAccountRequestDTO param);

    //2.3 잔액 조회
    ResponseEntity<InquiryBalanceResponseDTO> inquiryBalance(InquireBalanceRequestDTO param);

    //2.4 개인카드 승인내역조회.
    ResponseEntity<InquiryCreditCardHistoryResponseDTO> getCreditCardHistory(InquiryCreditCardHistoryRequestDTO param);

    //2.5 자기앞 수표 조회.
    ResponseEntity<InquiryCheckResponseDTO> getInquiryCheck(InquiryCheckRequestDTO param);

    //2.6 환율 조회.
    ResponseEntity<InquiryExchangeRateResponseDTO> getInquiryExchangeRate(InquiryExchangeRateRequestDTO param);

}
