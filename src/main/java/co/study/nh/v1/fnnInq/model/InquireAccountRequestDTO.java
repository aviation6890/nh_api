package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "예금주조회 Request DTO")
public class InquireAccountRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "은행코드", example = "은행코드를 입력해주세요.", notes = "농협은행:011, 상호금융:012", required = true)
    @JsonProperty(value = "Bncd")
    private String Bncd;

    @ApiModelProperty(value = "계좌번호", example = "계좌번호를 입력해주세요.", required = true)
    @JsonProperty(value = "Acno")
    private String Acno;

}
