package co.study.nh.v1.fnnInq.service.impl;

import co.study.nh.v1.fnnInq.model.*;
import co.study.nh.v1.fnnInq.service.FinancialInquiryService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@Slf4j
public class FinancialFinancialInquiryServiceImpl implements FinancialInquiryService {

    private static String INQUIRY_ACCOUNT_URL = "https://developers.nonghyup.com/InquireDepositorAccountNumber.nh";             // 2.1 예금주 조회.
    private static String INQUIRY_ORDER_ACCOUNT_URL = "https://developers.nonghyup.com/InquireDepositorOtherBank.nh";           // 2.2 타행 예금주 조회.
    private static String INQUIRY_BALANCE_URL = "https://developers.nonghyup.com/InquireBalance.nh";                            // 2.3 잔액 조회.
    private static String INQUIRY_CREDIT_CARD_HISTORY_RUL = "https://developers.nonghyup.com/InquireCreditCardAuthorizationHistory.nh";     // 2.4 잔액 조회
    private static String INQUIRY_CHECK_URL = "https://developers.nonghyup.com/InquireCashierCheck.nh";                         // 2.5 자기앞수표 조회.
    private static String INQUIRY_EXCHANGE_RATE_URL = "https://developers.nonghyup.com/InquireExchangeRate.nh";                 // 2.6 환율 조회.

    HttpHeaders httpHeaders = new HttpHeaders();
    RestTemplate restTemplate = new RestTemplate();

    /**
     * 2.1 예금주 조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<InquireAccountResponseDTO> getInquiryAccount(InquireAccountRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_ACCOUNT_URL);
        log.info("param ::{}", param);


        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<InquireAccountResponseDTO> exchange = restTemplate
                .exchange(INQUIRY_ACCOUNT_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        InquireAccountResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 2.2 타행 예금주 조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<InquireOrderAccountResponseDTO> getInquiryOrderAccount(InquireOrderAccountRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_ORDER_ACCOUNT_URL);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<InquireOrderAccountResponseDTO> exchange = restTemplate
                .exchange(INQUIRY_ORDER_ACCOUNT_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        InquireOrderAccountResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 2.3 잔액조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<InquiryBalanceResponseDTO> inquiryBalance(InquireBalanceRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_BALANCE_URL);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<InquiryBalanceResponseDTO> exchange = restTemplate
                .exchange(INQUIRY_BALANCE_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        InquiryBalanceResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 2.4 개인카드 승인내역조회.
     */
    @Override
    public ResponseEntity<InquiryCreditCardHistoryResponseDTO> getCreditCardHistory(InquiryCreditCardHistoryRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_CREDIT_CARD_HISTORY_RUL);
        log.info("param ::{}", param);

        Gson gson = new Gson();
        RestTemplate restTemplate = new RestTemplate();
        String paramJson = gson.toJson(param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<String> resultString = restTemplate
                .exchange(INQUIRY_CREDIT_CARD_HISTORY_RUL,
                        HttpMethod.POST,
                        new HttpEntity<String>(paramJson, httpHeaders),
                        String.class);

        ResponseEntity<InquiryCreditCardHistoryResponseDTO> result = new ResponseEntity<>(gson.fromJson(resultString.getBody(), InquiryCreditCardHistoryResponseDTO.class), HttpStatus.OK);

        return result;
    }

    /**
     * 2.5 자기앞 수표 조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<InquiryCheckResponseDTO> getInquiryCheck(InquiryCheckRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_CHECK_URL);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<InquiryCheckResponseDTO> exchange = restTemplate
                .exchange(INQUIRY_CHECK_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        InquiryCheckResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }




    /**
     * 2.6 환율조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<InquiryExchangeRateResponseDTO> getInquiryExchangeRate(InquiryExchangeRateRequestDTO param) {
        log.info("CALL API URL ::{}", INQUIRY_EXCHANGE_RATE_URL);
        log.info("param ::{}", param);

        Gson gson = new Gson();
        RestTemplate restTemplate = new RestTemplate();
        String paramJson = gson.toJson(param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<String> resultString = restTemplate
                .exchange(INQUIRY_EXCHANGE_RATE_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(paramJson, httpHeaders),
                        String.class);

        ResponseEntity<InquiryExchangeRateResponseDTO> result = new ResponseEntity<>(gson.fromJson(resultString.getBody(), InquiryExchangeRateResponseDTO.class), HttpStatus.OK);
        return result;
    }


}
