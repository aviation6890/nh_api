package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "환율조회 Request DTO")
public class InquiryExchangeRateRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "고시회차", example = "0001", required = true, notes = "0000 : 최신 고시환율 ,0001 : 1회차 테스트에서는 '0001' 고정값으로 입력")
    @JsonProperty(value = "Btb")
    private String Btb;

    @ApiModelProperty(value = "통화코드", example = "USD", notes = "default : 전체 USD:미국(달러), JPY:일본(엔) EUR:유럽연합(유로), CNY:중국(위안) 테스트에서는 'USD' 고정값으로 입력")
    @JsonProperty(value = "Crcd")
    private String Crcd;

    @ApiModelProperty(value = "조회일", example = "20191113", required = true, notes = "테스트에서는 '20191113' 고정값으로 입력")
    @JsonProperty(value = "Inymd")
    private String Inymd;

}
