package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "개인카드승인내역조회  Request DTO")
public class InquiryCreditCardHistoryRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "핀-카드", example = "핀-카드를 입력해주세요.", required = true)
    @JsonProperty(value = "FinCard")
    private String FinCard;

    @ApiModelProperty(value = "국내외사용구분", example = "1", required = true, notes = "1:국내 2:해외 테스트에서는 '1' 고정값으로 입력")
    @JsonProperty(value = "IousDsnc")
    private String IousDsnc;

    @ApiModelProperty(value = "조회시작일자", example = "20191105", required = true, notes = "최고1년 테스트에서는 '20191105' 고정값으로 입력")
    @JsonProperty(value = "Insymd")
    private String Insymd;

    @ApiModelProperty(value = "조회종료일자", example = "20191109", notes = "조회기간:최대3개월 테스트에서는 '20191109' 고정값으로 입력")
    @JsonProperty(value = "Ineymd")
    private String Inymd;

    @ApiModelProperty(value = "페이지번호", example = "1", notes = "default : 1 조회건수 15건 초과하는 경우 (페이지번호 + 1)하여 연속거래 수행 테스트에서는 '1' 고정값으로 입력")
    @JsonProperty(value = "PageNo")
    private String PageNo;

    @ApiModelProperty(value = "요청건수", example = "15", required = true, notes = "최대 요청건수 15건 - 페이지당 건수 테스트에서는 '15' 고정값으로 입력")
    @JsonProperty(value = "Dmcnt")
    private String Dmcnt;

}
