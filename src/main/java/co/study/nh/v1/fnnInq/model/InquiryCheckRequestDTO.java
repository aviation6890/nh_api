package co.study.nh.v1.fnnInq.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "자기앞수표조회 Request DTO")
public class InquiryCheckRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "수표발행 은행코드", example = "011", required = true, notes = "테스트에서는 '011' 고정값으로 입력")
    @JsonProperty(value = "ChisBncd")
    private String ChisBncd;

    @ApiModelProperty(value = "자기앞수표번호", example = "10000001", required = true, notes = "테스트에서는 '10000001' 고정값으로 입력")
    @JsonProperty(value = "CschNo")
    private String CschNo;

    @ApiModelProperty(value = "발행일자", example = "20190425", required = true, notes = "테스트에서는 '20190425' 고정값으로 입력")
    @JsonProperty(value = "Psymd")
    private String Psymd;

    @ApiModelProperty(value = "자기앞수표권종코드", example = "19", required = true, notes = "13:10만원, 14:30만원, 15:50만원, 16:100만원, 19:일반권 테스트에서는 '19' 고정값으로 입력")
    @JsonProperty(value = "Inymd")
    private String CschCtblCd;

    @ApiModelProperty(value = "수표발행 지로코드", example = "0110013", required = true, notes = "6~7자리 테스트에서는 '0110013' 고정값으로 입력")
    @JsonProperty(value = "ChisGicd")
    private String ChisGicd;

    @ApiModelProperty(value = "수표발행 지점코드", example = "000001", required = true, notes = "6자리 테스트에서는 '000001' 고정값으로 입력")
    @JsonProperty(value = "ChisBrcd")
    private String ChisBrcd;

    @ApiModelProperty(value = "수표액면금액", example = "1200000", required = true, notes = "테스트에서는 '1200000' 고정값으로 입력")
    @JsonProperty(value = "ChPvamt")
    private String ChPvamt;

}
