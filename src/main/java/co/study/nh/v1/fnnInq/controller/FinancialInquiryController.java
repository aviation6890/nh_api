package co.study.nh.v1.fnnInq.controller;

import co.study.nh.v1.fnnInq.model.*;
import co.study.nh.v1.fnnInq.service.FinancialInquiryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@Api(tags = {"2.금융조회"})
public class FinancialInquiryController {

    private final FinancialInquiryService financialInquiryService;

    @ApiOperation(value = "2.1 예금주 조회.", notes = "api 대략적인 설명 (완료/미완료/작업중)")
    @PostMapping(value = "/v1/InquireDepositorAccountNumber", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InquireAccountResponseDTO> inquiryAccount(@RequestBody InquireAccountRequestDTO param) {
        return financialInquiryService.getInquiryAccount(param);
    }

    @ApiOperation(value = "2.2 타행 예금주 조회.")
    @PostMapping(value = "/v1/InquireDepositorOtherBank", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InquireOrderAccountResponseDTO> inquiryOrderAccount(@RequestBody InquireOrderAccountRequestDTO param) {
        return financialInquiryService.getInquiryOrderAccount(param);
    }

    @ApiOperation(value = "2.3 잔액 조회.")
    @PostMapping(value = "/v1/InquireBalance", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InquiryBalanceResponseDTO> inquireBalance(@RequestBody InquireBalanceRequestDTO param) {
        return financialInquiryService.inquiryBalance(param);
    }

    @ApiOperation(value = "2.4 개인카드 승인내역 조회.")
    @PostMapping(value = "/v1/InquireCreditCardAuthorizationHistory", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InquiryCreditCardHistoryResponseDTO> creditCardHistory(@RequestBody InquiryCreditCardHistoryRequestDTO param) {
        ResponseEntity<InquiryCreditCardHistoryResponseDTO> creditCardHistory = financialInquiryService.getCreditCardHistory(param);

        log.info("creditCardHistory :: {}", creditCardHistory);

        return creditCardHistory;
    }

    @ApiOperation(value = "2.5 자기앞수표 조회.")
    @PostMapping(value = "/v1/InquireCashierCheck", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InquiryCheckResponseDTO> inquiryCheck(@RequestBody InquiryCheckRequestDTO param) {
        return financialInquiryService.getInquiryCheck(param);
    }

    @ApiOperation(value = "2.6 환율 조회.")
    @PostMapping(value = "/v1/InquireExchangeRate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InquiryExchangeRateResponseDTO> inquiryExchangeRate(@RequestBody InquiryExchangeRateRequestDTO param) {
        return financialInquiryService.getInquiryExchangeRate(param);
    }

}
