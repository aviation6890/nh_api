package co.study.nh.v1.easypay.service.impl;

import co.study.nh.v1.easypay.model.*;
import co.study.nh.v1.easypay.service.EasyPayService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class EasyPayServiceImpl implements EasyPayService {

    private static String WITHDRAW_URL = "https://developers.nonghyup.com/DrawingTransfer.nh";                          // 3.1 간편결제 - 출금이제.
    private static String NH_DEPOSIT_URL = "https://developers.nonghyup.com/ReceivedTransferAccountNumber.nh";          // 3.2 간편결제 - 농협입금이체.
    private static String ORDER_BANK_DEPOSIT_URL = "https://developers.nonghyup.com/ReceivedTransferOtherBank.nh";      // 3.3 간편결제 - 타행입금이체.
    private static String TRANSACTION_HISTORY_URL = "https://developers.nonghyup.com/InquireTransactionHistory.nh";     // 3.4 간편결제 - 거래내역조회.

    HttpHeaders httpHeaders = new HttpHeaders();
    RestTemplate restTemplate = new RestTemplate();

    /**
     * 3.1 출금이체
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<WithdrawResponseDTO> withdraw(WithdrawRequestDTO param) {
        log.info("CALL API URL ::{}", WITHDRAW_URL);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<WithdrawResponseDTO> exchange = restTemplate
                .exchange(WITHDRAW_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        WithdrawResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 3.2 농협입금이체.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<NhWithdrawResponseDTO> nhWithdraw(NhWithdrawRequestDTO param) {
        log.info("CALL API URL ::{}", NH_DEPOSIT_URL);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<NhWithdrawResponseDTO> exchange = restTemplate
                .exchange(NH_DEPOSIT_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        NhWithdrawResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 3.3 타행입금이체
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<OrderBankDepositResponseDTO> orderBankWithdraw(OrderBankDepositRequestDTO param) {
        log.info("CALL API URL ::{}", ORDER_BANK_DEPOSIT_URL);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<OrderBankDepositResponseDTO> exchange = restTemplate
                .exchange(ORDER_BANK_DEPOSIT_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        OrderBankDepositResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }

    /**
     * 3.4 거래내역조회.
     *
     * @param param
     * @return
     */
    @Override
    public ResponseEntity<TransactionHistoryResponseDTO> transactionHistory(TransactionHistoryRequestDTO param) {
        log.info("CALL API URL ::{}", TRANSACTION_HISTORY_URL);
        log.info("param ::{}", param);

        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<TransactionHistoryResponseDTO> exchange = restTemplate
                .exchange(TRANSACTION_HISTORY_URL,
                        HttpMethod.POST,
                        new HttpEntity<String>(new Gson().toJson(param), httpHeaders),
                        TransactionHistoryResponseDTO.class);
        log.info("exchange :: {}", exchange);

        return exchange;
    }
}
