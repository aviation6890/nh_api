package co.study.nh.v1.easypay.model;

import co.study.nh.v1.common.model.APICommonResponseHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "간편결제-거래내역조회 Response DTO")
public class TransactionHistoryResponseDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonResponseHeader Header;

    @ApiModelProperty(value = "연속데이터여부", required = true, notes = "Y:이후데이터있음")
    @JsonProperty(value = "CtntDataYn")
    private String CtntDataYn;

    @Deprecated
    @ApiModelProperty(value = "총건수", required = true, notes = "미사용")
    @JsonProperty(value = "TotCnt")
    private String TotCnt;

    @ApiModelProperty(value = "조회총건수", required = true)
    @JsonProperty(value = "Iqtcnt")
    private String Iqtcnt;

    @ApiModelProperty(value = "거래내역목록", required = true, notes = "최대 100건")
    @JsonProperty(value = "REC")
    private String REC;

    @ApiModelProperty(value = "거래일자", required = true)
    @JsonProperty(value = "Trdd")
    private String Trdd;

    @ApiModelProperty(value = "거래시각", required = true)
    @JsonProperty(value = "Txtm")
    private String Txtm;

    @ApiModelProperty(value = "입금출금구분", example = "011", required = true, notes = "1:신규(입금) 2:입금 3:출금 4:해지(출금)")
    @JsonProperty(value = "MnrcDrotDsnc")
    private String MnrcDrotDsnc;

    @ApiModelProperty(value = "거래금액", required = true)
    @JsonProperty(value = "Tram")
    private String Tram;

    @ApiModelProperty(value = "거래후잔액", required = true)
    @JsonProperty(value = "AftrBlnc")
    private String AftrBlnc;

    @ApiModelProperty(value = "거래후계좌잔액", required = true)
    @JsonProperty(value = "TrnsAfAcntBlncSmblCd")
    private String TrnsAfAcntBlncSmblCd;

    @ApiModelProperty(value = "적요", required = true)
    @JsonProperty(value = "Smr")
    private String Smr;

    @ApiModelProperty(value = "최급기관코드", required = true)
    @JsonProperty(value = "HnisCd")
    private String HnisCd;

    @ApiModelProperty(value = "취급지점코드", required = true)
    @JsonProperty(value = "HnbrCd")
    private String HnbrCd;

    @ApiModelProperty(value = "취소여부", required = true, notes = "정상:0, 취소:1")
    @JsonProperty(value = "Ccyn")
    private String Ccyn;

    @ApiModelProperty(value = "거래고유번호", required = true)
    @JsonProperty(value = "Tuno")
    private String Tuno;

    @ApiModelProperty(value = "통장인자내용", required = true)
    @JsonProperty(value = "BnprCntn")
    private String BnprCntn;

}
