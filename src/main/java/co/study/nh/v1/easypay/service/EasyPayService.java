package co.study.nh.v1.easypay.service;

import co.study.nh.v1.easypay.model.*;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountRequestDTO;
import co.study.nh.v1.nhinit.model.pinaccount.IssuePinAccountResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface EasyPayService {

    // 3.1 간편결제-출금이체.
    ResponseEntity<WithdrawResponseDTO> withdraw(WithdrawRequestDTO param);

    // 3.2 간편결제-농협입금이체.
    ResponseEntity<NhWithdrawResponseDTO> nhWithdraw(NhWithdrawRequestDTO param);

    // 3.2 간편결제-농협입금이체.
    ResponseEntity<OrderBankDepositResponseDTO> orderBankWithdraw(OrderBankDepositRequestDTO param);

    // 3.4 간편결제-거래내역조회.
    ResponseEntity<TransactionHistoryResponseDTO> transactionHistory(TransactionHistoryRequestDTO param);
}
