package co.study.nh.v1.easypay.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "간편결제-거래내역조회 Request DTO")
public class TransactionHistoryRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "은행코드", example = "011", required = true, notes = "농협은행:011, 상호금융:012")
    @JsonProperty(value = "Bncd")
    private String Bncd;

    @ApiModelProperty(value = "계좌번호", required = true)
    @JsonProperty(value = "Acno")
    private String Acno;

    @ApiModelProperty(value = "조회시작일자", required = true, notes = "최고1년")
    @JsonProperty(value = "Insymd")
    private String Insymd;

    @ApiModelProperty(value = "조회종료일자", required = true, notes = "조회기간: 최대 3개월")
    @JsonProperty(value = "Ineymd")
    private String Ineymd;

    @Deprecated
    @ApiModelProperty(value = "거래구분", example = "A", notes = "A:전체 M:입금 D:출금, default : A")
    @JsonProperty(value = "TrnsDsnc")
    private String TrnsDsnc;

    @Deprecated
    @ApiModelProperty(value = "정렬순서", example = "ASC", notes = "ASC:오름차순, DESC:내림차순, default:ASC")
    @JsonProperty(value = "Lnsq")
    private String Lnsq;

    @Deprecated
    @ApiModelProperty(value = "페이지번호", example = "1", notes = "default : 1 조회건수 100건 초과하는 경우 (페이지번호 + 1)하여 연속거래 수행")
    @JsonProperty(value = "PageNo")
    private String PageNo;

    @Deprecated
    @ApiModelProperty(value = "요청건수", required = true, notes = "최대 요청건수 100건 - 페이지당 건수")
    @JsonProperty(value = "Dmcnt")
    private String Dmcnt;
}
