package co.study.nh.v1.easypay.controller;

import co.study.nh.v1.easypay.model.*;
import co.study.nh.v1.easypay.service.EasyPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@Api(tags = {"3. 간편결제"})
public class EasyPayController {

    private final EasyPayService easyPayService;

    @ApiOperation(value = "3.1 출금이제.")
    @PostMapping(value = "/v1/DrawingTransfer", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WithdrawResponseDTO> easyPayWithdraw(@RequestBody WithdrawRequestDTO param) {
        return easyPayService.withdraw(param);
    }

    @ApiOperation(value = "3.2 농협입금이체.")
    @PostMapping(value = "/v1/ReceivedTransferAccountNumber", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NhWithdrawResponseDTO> nhDeposit(@RequestBody NhWithdrawRequestDTO param) {
        return easyPayService.nhWithdraw(param);
    }

    @ApiOperation(value = "3.3 타행입금이체.")
    @PostMapping(value = "/v1/ReceivedTransferOtherBank", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderBankDepositResponseDTO> easyPayWithdraw(@RequestBody OrderBankDepositRequestDTO param) {
        return easyPayService.orderBankWithdraw(param);
    }

    @ApiOperation(value = "3.4 거래내역조회.")
    @PostMapping(value = "/v1/InquireTransactionHistory", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionHistoryResponseDTO> nhDeposit(@RequestBody TransactionHistoryRequestDTO param) {
        return easyPayService.transactionHistory(param);
    }
}
