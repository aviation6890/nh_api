package co.study.nh.v1.easypay.model;

import co.study.nh.v1.common.model.APICommonHeader;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "간편결제-농협입금이체 Request DTO")
public class OrderBankDepositRequestDTO {

    @ApiModelProperty(value = "API Header 공통부.")
    @JsonProperty(value = "Header")
    private APICommonHeader Header;

    @ApiModelProperty(value = "은행코드", example = "011", required = true, notes = "농협은행:011, 상호금융:012")
    @JsonProperty(value = "Bncd")
    private String Bncd;

    @ApiModelProperty(value = "계좌번호", required = true)
    @JsonProperty(value = "Acno")
    private String Acno;

    @ApiModelProperty(value = "거래금액", required = true)
    @JsonProperty(value = "Tram")
    private String Tram;

    @ApiModelProperty(value = "출금계좌인자내용", notes = "출금계좌 통장적용 인자내용")
    @JsonProperty(value = "DractOtlt")
    private String DractOtlt;

    @Deprecated
    @ApiModelProperty(value = "입금계좌인자내용", notes = "입금고객계좌 통장적용 인자내용 ex) 핀테크기업명 or 핀테크서비스명", required = true)
    @JsonProperty(value = "MractOtlt")
    private String MractOtlt;
}
